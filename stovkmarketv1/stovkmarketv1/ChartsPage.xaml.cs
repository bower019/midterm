﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;
using Entry = Microcharts.Entry;
using Xamarin.Forms.Xaml;



namespace stovkmarketv1
{

	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChartsPage : ContentPage
	{
        int a = 0;
        public ChartsPage ()
		{
            InitializeComponent();

        }

        void Handle_Appearing(object sender, System.EventArgs e)
        {
            var CData = HomePage.SData;
            CreateCharts();

        }

        private void CreateCharts()
        {
            var entryList = new List<Entry>();
            if (HomePage.SData != null  )
            {
               foreach(var stockday in HomePage.SData.TimeSeriesDaily.Values)
                {
                    a = a + 1;
                    if (a % 4 == 0)

                    {

                        var Temp2 = new Entry(float.Parse(stockday.High))
                        {
                            Color = SKColor.Parse("#4e42f4"),
                            ValueLabel = stockday.High,

                        };
                        entryList.Add(Temp2);
                        entryList.Reverse();


                    }
                }

                // for (int a=0; a< 56; a++) 
                //{ 
                //    if (a % 2 == 0) 
                //    {
                //        temp = float.Parse(HomePage.SData.TimeSeriesDaily.Values.High);
                //    }
                //  var Temp2 = new Entry(temp)
                //   {
                //     Color = SKColor.Parse("#4e42f4"),
                //     ValueLabel = HomePage.SData.High,

                //   }
                //    entryList.Add(Temp2);
                //    entryList.Reverse();
                //}

            }
            var lineChart = new LineChart()
            {
                Entries = entryList,
                LineMode = LineMode.Straight,
                LineSize = 8,
                PointMode = PointMode.Square,
                PointSize = 18,
            };



            Chart1.Chart = lineChart;
        }


        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Chart1.IsVisible = !Chart1.IsVisible;
        }
    }
}


